﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operatory_logiczne
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Koniunkcja czyl && */
            /*Console.WriteLine((5 > 7) && (9>4) && (4!=4));
            Console.Read();*/
            /*Alternatywa (lub) || */
            /*Console.WriteLine((4>7) || (15>7));
            Console.Read();*/
            /*Zaprzeczenie/Negacja ! */
            /*Console.WriteLine(!(5 > 7) && (9 > 4) && (4 == 4));
            Console.Read();*/
            /*Operatory bitowe*/
            Console.WriteLine(10 ^ 2);
            Console.Read();
        }
    }
}
